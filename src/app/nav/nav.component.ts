import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { AuthService } from './../auth.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent {

  title: string = 'link1'; ////////////////////change to main/////////////////////
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

    constructor(private breakpointObserver: BreakpointObserver,public authService:AuthService, location: Location, router: Router) {
      router.events.subscribe(val => {
        if (location.path() == "/link1") { ////////////////////change to main/////////////////////
          this.title = 'Link1';////////////////////change to main/////////////////////
          console.log(val);        
        } 
        else if (location.path() == "/signup"){
              this.title = "Sign up form"; 
        }

         else{
          this.title = "Link3";////////////////////change to link3/////////////////////
        }
       
      });   
    }
    }
    
