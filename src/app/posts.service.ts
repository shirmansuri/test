import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Post } from './interfaces/post';
// import { User } from './interfaces/user';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class PostsService {
  private API = "https://jsonplaceholder.typicode.com/posts"
  private commentAPI = "https://jsonplaceholder.typicode.com/posts/1/comments"

  constructor(private db:AngularFirestore,
    private http:HttpClient) { }

    getPost(): Observable<Post[]>
 {
    // return this.db.collection<Post>("posts").valueChanges();
     return this.http.get<Post[]>(`${this.API}`);
  }

  getComment(): Observable<Comment>
 {
  
    return this.http.get<Comment>(`${this.commentAPI}`);
  
  }
  updatelikes(userId:string, id:string,like:number){
    this.db.doc(`users/${userId}/posts/${id}`).update(
       {
         like:like
       }
     )
   }

  
}

