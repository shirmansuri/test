import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { PostsService } from '../posts.service';
import { AuthService } from '../auth.service';


@Component({
  selector: 'app-saved-posts',
  templateUrl: './saved-posts.component.html',
  styleUrls: ['./saved-posts.component.css']
})
export class SavedPostsComponent implements OnInit {

  posts$:Observable<any>;
  userId:string;
  like:number;

  constructor(private postservice:PostsService,public auth:AuthService) { }
  likeIt(id:string,likes:number){
    this.like = likes + 1 ; 
    this.postservice.updatelikes(this.userId,id,this.like)
  }

  ngOnInit() {
  }

}
