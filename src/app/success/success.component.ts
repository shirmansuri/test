import { Component, OnInit } from '@angular/core';
import { AuthService } from './../auth.service';

@Component({
  selector: 'app-success',
  templateUrl: './success.component.html',
  styleUrls: ['./success.component.css']
})
export class SuccessComponent implements OnInit {
  userDisplayEmail = '';
  
  constructor(public auth:AuthService) { }
  userId:string;
  usermail:string;

  ngOnInit() {
    this.auth.user.subscribe(
      user => {
        this.userId = user.uid;
        this.usermail = user.email;
            }
    )
  }

}
