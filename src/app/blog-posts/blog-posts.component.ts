import { Component, OnInit } from '@angular/core';
import { PostsService } from '../posts.service';

@Component({
  selector: 'app-blog-posts',
  templateUrl: './blog-posts.component.html',
  styleUrls: ['./blog-posts.component.css']
})
export class BlogPostsComponent implements OnInit {
  panelOpenState = false;
  posts$
  comments$
  userId:string;

   constructor(private postservice:PostsService) { }

  ngOnInit() {
    this.posts$ = this.postservice.getPost();
    this.comments$ = this.postservice.getComment();
  }

}
