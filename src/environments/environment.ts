// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyAJQH7BtiY2K4YmYf3F1dC-1NsvVUAxwxY",
    authDomain: "test-92f77.firebaseapp.com",
    databaseURL: "https://test-92f77.firebaseio.com",
    projectId: "test-92f77",
    storageBucket: "test-92f77.appspot.com",
    messagingSenderId: "704891706461",
    appId: "1:704891706461:web:ad297a19d9ecaab37241b4",
    measurementId: "G-QS7B9QS9BV"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
